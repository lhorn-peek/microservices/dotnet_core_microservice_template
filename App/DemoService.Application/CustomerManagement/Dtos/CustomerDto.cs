﻿using System;

namespace DemoService.Application.CustomerManagement.Dtos
{
    public class CustomerDto
    {
        public string CustomerId { get; set; }

        public string CustomerName { get; set; }

        public int TotalAccounts { get; set; }

        public DateTime LastTransactionOn { get; set; }
    }
}
