﻿using System;
using System.Linq;
using DemoService.Core.Customers;

namespace DemoService.Application.CustomerManagement.Dtos
{
    public class CustomerDtoMapper
    {
        public CustomerDto MapFrom(Customer customer)
        {
            var dto = new CustomerDto
            {
                CustomerId = customer.Id,
                CustomerName = customer.Name,
                TotalAccounts = customer.Accounts.Count,
                LastTransactionOn = GetLastTransactionDate(customer)
            };

            return dto;
        }

        private static DateTime GetLastTransactionDate(Customer customer)
        {
            var transactions = customer.Accounts.SelectMany(a => a.Transactions).ToList();

            var lastTransactionDate = transactions.Max(t => t.DateCreated);

            return lastTransactionDate;
        }
    }
}
