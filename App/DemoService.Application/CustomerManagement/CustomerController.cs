﻿using DemoService.Application.CustomerManagement.Commands;
using DemoService.Application.CustomerManagement.Dtos;

//decoupled controller from Asp.net core framework for easy portability
namespace DemoService.Application.CustomerManagement
{


    public class CustomerController
    {
        #region init

        private readonly ICustomerManager _customerManager;

        public CustomerController(ICustomerManager customerManager)
        {
            _customerManager = customerManager;
        }

        #endregion

        //GET
        public CustomerDto GetCustomer(string id)
        {
            var customerDto = _customerManager.GetCustomer(id);

            return customerDto;
        }

        //POST
        public void ChargeCustomerAccount(ChargeCustomerCommand command)
        {
            _customerManager.ChargeCustomer(command);
        }
    }
}
