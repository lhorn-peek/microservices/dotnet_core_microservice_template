﻿using DemoService.Application.CustomerManagement.Commands;
using DemoService.Application.CustomerManagement.Dtos;
using DemoService.Management.Commands;

namespace DemoService.Application.CustomerManagement
{
    public interface ICustomerManager
    {
        void ChargeCustomer(ChargeCustomerCommand command);
        void CreateNewCustomer(NewCustomerCommand command);
        CustomerDto GetCustomer(string customerId);
    }
}