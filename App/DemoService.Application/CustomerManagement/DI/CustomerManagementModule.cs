﻿using DemoService.Application.CustomerManagement.Dtos;
using DemoService.Core.Customers.Interface;
using DemoService.Core.Customers.Service;
using DemoService.Infrastructure.Domain.Customers;
using DemoService.Persistence.Sql;
using DemoService.Persistence.Sql.Repositories;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace DemoService.Application.CustomerManagement.DI
{
    public class CustomerManagementModule : IPackage
    {
        private readonly ManagementModuleSettings _settings;

        public CustomerManagementModule(ManagementModuleSettings settings)
        {
            _settings = settings;
        }

        public void RegisterServices(Container container)
        {
            container.Register(() => new DemoDbContext(_settings.DbConnectionString));

            container.Register<ICustomerRepository, CustomerRepository>();

            container.Register<ICustomerKeyGenerator, CustomerKeyGenerator>();

            container.Register<ICustomerValidator, CustomerValidator>();

            container.Register<IDebitValidator, AccountDebitValidator>();

            container.Register<ICustomerService, CustomerService>();

            container.Register<CustomerDtoMapper>();

            container.Register<ICustomerManager, CustomerManager>();
        }
    }
}
