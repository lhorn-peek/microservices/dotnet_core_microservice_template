﻿namespace DemoService.Application.CustomerManagement.DI
{
    public class ManagementModuleSettings
    {
        public string DbConnectionString { get; set; }
    }
}
