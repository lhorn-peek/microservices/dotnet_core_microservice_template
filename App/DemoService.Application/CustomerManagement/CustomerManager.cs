﻿using DemoService.Application.CustomerManagement.Commands;
using DemoService.Application.CustomerManagement.Dtos;
using DemoService.Core.Customers.Service;
using DemoService.Management.Commands;

namespace DemoService.Application.CustomerManagement
{
    public class CustomerManager : ICustomerManager
    {
        #region init

        private readonly ICustomerService _customerService;
        private readonly CustomerDtoMapper _dtoMapper;

        public CustomerManager(ICustomerService customerService, CustomerDtoMapper dtoMapper)
        {
            _customerService = customerService;
            _dtoMapper = dtoMapper;
        }

        #endregion

        public void CreateNewCustomer(NewCustomerCommand command)
        {
            _customerService.CreateCustomer(command.Name);
        }

        public void ChargeCustomer(ChargeCustomerCommand command)
        {
            _customerService.DebitAccount(command.CustomerId, command.Amount, command.AccountNumber);
        }

        public CustomerDto GetCustomer(string customerId)
        {
            var customer = _customerService.GetCustomer(customerId);

            var dto = _dtoMapper.MapFrom(customer);

            return dto;
        }
    }
}
