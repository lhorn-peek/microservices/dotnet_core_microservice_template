﻿namespace DemoService.Management.Commands
{
    public class NewCustomerCommand
    {
        public string Name { get; set; }
    }
}