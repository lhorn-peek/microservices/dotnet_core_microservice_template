﻿namespace DemoService.Application.CustomerManagement.Commands
{
    public class ChargeCustomerCommand
    {
        public string CustomerId { get; set; }

        public string AccountNumber { get; set; }

        public decimal Amount { get; set; }
    }
}
