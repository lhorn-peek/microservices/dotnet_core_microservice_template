﻿namespace DemoService.Core.Customers.Interface
{
    public interface ICustomerKeyGenerator
    {
        string NewId();
    }
}
