﻿namespace DemoService.Core.Customers.Interface
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);

        void UpdateCustomer(Customer customer);

        Customer GetCustomer(string customerId);

        void Commit();
    }
}
