﻿namespace DemoService.Core.Customers.Interface
{
    public interface ICustomerValidator
    {
        void ValidateCustomer(Customer customer);
    }
}
