﻿namespace DemoService.Core.Customers.Interface
{
    public interface IDebitValidator
    {
        bool Validate(Account account, decimal amount);
    }
}