﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoService.Core.Customers.Interface;

namespace DemoService.Core.Customers
{
    public class Customer
    {
        #region init

        public Customer(string name, ICustomerKeyGenerator keyGenerator)
        {
            Id = keyGenerator.NewId();
            Name = name;
            Accounts = new List<Account>();
        }

        #endregion


        public string Id { get; }

        public string Name { get; private set; }

        public IList<Account> Accounts { get; }

        public void DebitAccount(string accountNumber, decimal amount, IDebitValidator debitValidator)
        {
            var account = Accounts?.FirstOrDefault(a => a.AccountNumber == accountNumber);

            if(account == null)
                throw new ArgumentException($"No account exists from {accountNumber}");

            var isValidDebit = debitValidator.Validate(account, amount);

            if (!isValidDebit)
                throw new InvalidOperationException($"Cannot debit account {accountNumber}");

            account.DebitAccount(amount);
        }

        public void ChangeName(string newName)
        {
            SetName(newName);
        }

        protected void SetName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            Name = Name;
        }
    }
}
