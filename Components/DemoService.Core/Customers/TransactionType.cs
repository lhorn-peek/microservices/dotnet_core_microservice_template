﻿namespace DemoService.Core.Customers
{
    public enum TransactionType
    {
        Debit = 0,
        Credit = 1
    }
}
