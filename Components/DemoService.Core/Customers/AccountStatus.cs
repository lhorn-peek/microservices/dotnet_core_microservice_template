﻿namespace DemoService.Core.Customers
{
    public enum AccountStatus
    {
        Active = 0,
        Delinquent = 1
    }
}