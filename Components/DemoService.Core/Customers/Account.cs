﻿using System.Collections.Generic;

namespace DemoService.Core.Customers
{
    public class Account
    {
        #region init

        public Account(string newAccountNumber, Customer owner, decimal initialDeposit)
        {
            AccountNumber = newAccountNumber;
            Owner = owner;
            AccountStatus = AccountStatus.Active;
            Transactions = new List<Transaction>();

            CreditAccount(initialDeposit);
        }

        #endregion

        public string AccountNumber { get; }

        public Customer Owner { get; }

        public decimal Balance { get; private set; }

        public AccountStatus AccountStatus { get; }

        public int? DaysPastDue { get; private set; }

        public IList<Transaction> Transactions { get; }

        internal void DebitAccount(decimal amount)
        {
            Balance = Balance - amount;

            Transactions.Add(new Transaction(amount, TransactionType.Debit));
        }

        internal void CreditAccount(decimal amount)
        {
            Balance = Balance + amount;

            Transactions.Add(new Transaction(amount, TransactionType.Credit));
        }
    }
}
