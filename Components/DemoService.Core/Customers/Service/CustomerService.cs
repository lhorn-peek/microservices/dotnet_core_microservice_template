﻿using DemoService.Core.Customers.Interface;

namespace DemoService.Core.Customers.Service
{
    public class CustomerService : ICustomerService
    {
        #region init

        private readonly ICustomerRepository _customerRepository;
        private readonly ICustomerValidator _customerValidator;
        private readonly ICustomerKeyGenerator _keyGenerator;
        private readonly IDebitValidator _debitValidator;

        public CustomerService(ICustomerRepository customerRepository, ICustomerValidator customerValidator, 
            ICustomerKeyGenerator keyGenerator, IDebitValidator debitValidator)
        {
            _customerRepository = customerRepository;
            _customerValidator = customerValidator;
            _keyGenerator = keyGenerator;
            _debitValidator = debitValidator;
        }

        #endregion

        public void CreateCustomer(string name)
        {
            var newCustomer = new Customer(name, _keyGenerator);

            _customerRepository.AddCustomer(newCustomer);

            _customerRepository.Commit();
        }

        public void DebitAccount(string customerId, decimal amount, string accountNumber)
        {
            var customer = _customerRepository.GetCustomer(customerId);

            _customerValidator.ValidateCustomer(customer);

            customer.DebitAccount(accountNumber, amount, _debitValidator);

            _customerRepository.UpdateCustomer(customer);

            _customerRepository.Commit();
        }

        public Customer GetCustomer(string id)
        {
            var customer = _customerRepository.GetCustomer(id);

            return customer;
        }
    }
}
