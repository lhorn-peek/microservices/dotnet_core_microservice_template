namespace DemoService.Core.Customers.Service
{
    public interface ICustomerService
    {
        void CreateCustomer(string name);
        void DebitAccount(string customerId, decimal amount, string accountNumber);
        Customer GetCustomer(string id);
    }
}
