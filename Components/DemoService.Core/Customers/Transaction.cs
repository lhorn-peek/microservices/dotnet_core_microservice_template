﻿using System;

namespace DemoService.Core.Customers
{
    public class Transaction
    {
        #region init

        public Transaction(decimal amount, TransactionType transactionType)
        {
            Amount = amount;
            TransactionType = transactionType;
            DateCreated = DateTime.UtcNow;
        }

        #endregion

        public int Id { get; set; }

        public decimal Amount { get; }

        public TransactionType TransactionType { get; }

        public DateTime DateCreated { get; }

        public Account ForAccount { get; set; }
    }
}
