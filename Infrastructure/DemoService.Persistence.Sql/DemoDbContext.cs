﻿using DemoService.Core.Customers;
using Microsoft.EntityFrameworkCore;

namespace DemoService.Persistence.Sql
{
    public class DemoDbContext : DbContext
    {
        private readonly string _connection;

        public DemoDbContext(string connection)
        {
            _connection = connection;
        }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // C U S T O M E R
            modelBuilder.Entity<Customer>().HasKey(c => c.Id);

            modelBuilder.Entity<Customer>()
                .Property(t => t.Name).IsRequired();

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Accounts)
                .WithOne(a => a.Owner);


            // A C C O U N T
            modelBuilder.Entity<Account>().HasKey(c => c.AccountNumber);

            modelBuilder.Entity<Account>()
                .Property(t => t.Balance).IsRequired();

            modelBuilder.Entity<Account>()
                .Property(t => t.AccountStatus).IsRequired();

            modelBuilder.Entity<Account>()
                .Property(t => t.DaysPastDue);

            modelBuilder.Entity<Account>()
                .HasMany(c => c.Transactions)
                .WithOne(t => t.ForAccount);

            // T R A N S A C T I O N
            modelBuilder.Entity<Transaction>().HasKey(c => c.Id);

            modelBuilder.Entity<Transaction>()
                .Property(t => t.Id).UseSqlServerIdentityColumn();

            modelBuilder.Entity<Transaction>()
                .Property(t => t.TransactionType).IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(t => t.DateCreated).IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(t => t.Amount).IsRequired();
        }
    }
}
