﻿using DemoService.Core.Customers;
using DemoService.Core.Customers.Interface;

namespace DemoService.Persistence.Sql.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        #region init

        private readonly DemoDbContext _dbContext;

        public CustomerRepository(DemoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        public void AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            _dbContext.Customers.Update(customer);
        }

        public Customer GetCustomer(string customerId)
        {
            var customer = _dbContext.Customers.Find(customerId);

            return customer;
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}
