﻿using DemoService.Core.Customers;
using DemoService.Core.Customers.Interface;

namespace DemoService.Infrastructure.Domain.Customers
{
    public class AccountDebitValidator : IDebitValidator
    {
        public bool Validate(Account account, decimal amount)
        {
            if (account == null) return false;

            if (account.AccountStatus == AccountStatus.Delinquent) return false;

            if (account.DaysPastDue >= 30) return false;

            return (account.Balance - amount) > 0;
        }
    }
}
