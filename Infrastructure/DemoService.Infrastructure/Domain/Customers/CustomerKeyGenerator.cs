﻿using System;
using DemoService.Core.Customers.Interface;

namespace DemoService.Infrastructure.Domain.Customers
{
    public class CustomerKeyGenerator : ICustomerKeyGenerator
    {
        public string NewId()
        {
            var id = Guid.NewGuid();

            return id.ToString();
        }
    }
}
