﻿using System;
using DemoService.Core.Customers;
using DemoService.Core.Customers.Interface;

namespace DemoService.Infrastructure.Domain.Customers
{
    public class CustomerValidator : ICustomerValidator
    {
        public void ValidateCustomer(Customer customer)
        {
            if(customer == null)
                throw new ArgumentException(nameof(customer));
        }
    }
}
